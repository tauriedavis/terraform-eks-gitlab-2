#
# Variables Configuration
#

variable "cluster-name" {
  type    = "string"
  description = "The name of your cluster. This name will be merged into other, related AWS services too."
  default = "gitlab-eks"
}

variable "aws_region" {
  type = "string"
  default = "eu-central-1"
  description = "The AWS region to use for the cluster"
}

variable "gitlab-token" {
  type = "string"
  description = "The user access token to manage GitLab"
}

variable "gitlab-project-id" {
  type = number
  description = "The GitLab project ID to attach the cluster to"
}

variable "cluster-environment" {
  type = "string"
  description = "The environment for deployments"
  default = "*"
}